import apiClient from "../helper/apiClinet";
class UserService {
  getAllBook = () => apiClient().get("book");
  getAllAuthor = () => apiClient().get("author");
  addNewBook = (book) => apiClient().post("book", book);
  addNewAuthor = (auhtor) => apiClient().post("author", auhtor);
  editBook = (id, book) => apiClient().put(`book/${id}`, book);
  editAuthor = (id, auhtor) => apiClient().put(`author/${id}`, auhtor);
  getBookAuthorById = (id) => apiClient().get(`book/${id}`);
}
export default new UserService();
