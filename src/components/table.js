import { Table } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
export const TableDetails = () => {
  const dispatch = useDispatch();
  const { bookDetails } = useSelector((state) => state.main);
console.log(bookDetails,"bookDetails")
  return (
    <>
      {bookDetails && (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Book Name</th>
              <th>ISBN</th>
              <th>Author FirtName</th>
              <th>Author LastName</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>{bookDetails.name}</td>
              <td>{bookDetails.isbn}</td>
              <td>{bookDetails.authorDetails.firstName}</td>
              <td>{bookDetails.authorDetails.lastName}</td>
            </tr>
          </tbody>
        </Table>
      )}
    </>
  );
};
