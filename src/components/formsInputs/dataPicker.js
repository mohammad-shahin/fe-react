import { useFormContext } from "react-hook-form";
export const DataPicker = ({title,arabicTitle})=>{
    const {register} = useFormContext();
    return(
        <>
            <label for={title} className="form-label">{arabicTitle}</label>
            <div className="form-outline datepicker" data-mdb-inline="true">
                <input 
                    style={{textAlign:"right"}} 
                    type="date" 
                    className="form-control" 
                    id="exampleDatepicker2"
                    {...register(title, {required:true})} 
                    />
            </div>
        </>
        )
}