
import { useFormContext } from "react-hook-form";
export const Input = ({title,arabicTitle,value,disabled,isDispaly})=>{
    const {register} = useFormContext();

    return(
        <>
            <label for={title} className="form-label">{arabicTitle}</label>
            <input
                type = "text"
                className = "form-control"
                id = {title}
                value = { value && value}
                disabled = { isDispaly ? true : false }
                {...register(title, {required:true})}
            />
            <div className="invalid-feedback"> يرجى إدخال اسم أول صحيح. </div>
        </>
        )
}