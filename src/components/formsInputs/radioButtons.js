import { useFormContext } from "react-hook-form";
export const RadioButtons = ({title,items,arabicTitles,isDispaly})=>{
    const {register} = useFormContext();
    return(
        <>
            <label for="state" className="form-label">{arabicTitles}</label>
            {items && items.map((item,index)=>(
            <div key={index} className="form-check form-check-inline">
                <input
                    className="form-check-input"
                    type="radio"
                    name={title}
                    id={title}
                    disabled = { isDispaly ? true : false }
                    {...register(title)} value={item}/>
                <label className="form-check-label" for="inlineRadio1">{item}</label>
            </div>
            ))}
        </>
        )
}