
import { useFormContext } from "react-hook-form";
export const TextArea = ({title,arabicTitle,isDispaly})=>{
    const {register} = useFormContext();
    return(
        <>
            <label for="floatingTextarea2" className="form-label" >{arabicTitle}</label>
            <textarea
                className="form-control borderInput"
                id="typeOfwork"
                disabled = { isDispaly ? true : false }
                {...register(title)}
                style={{height: "100px"}}/>
        </>
        )
}