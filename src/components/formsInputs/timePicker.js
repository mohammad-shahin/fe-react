import { useFormContext } from "react-hook-form";
export const TimePikcer = ({title,arabicTitle,isDispaly})=>{
    const {register} = useFormContext();
    return(
        <>
            <label for={title} className="form-label">{arabicTitle}</label>
            <input
                type="time"
                id="depatureTime"
                className="form-control"
                disabled = { isDispaly ? true : false }
                {...register(title, {required:true})}
            />
        </>
        )
}