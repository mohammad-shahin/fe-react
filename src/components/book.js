import { ListGroup, Button } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { loadAllBook, addBook, editBook, bookAuthor } from "../state/reducers/actionsIndex";
import { useForm } from "react-hook-form";
import { TableDetails } from './table';
export const Book = () => {
  const dispatch = useDispatch();
  const { books, bookDetails } = useSelector((state) => state.main);
  const { register, handleSubmit, setValue, reset } = useForm();
  const [edit, setEdit] = useState(false);
  const [show, setShow] = useState(false);
  console.log(bookDetails)
  const onSubmit = (data) => {
    if(edit) {
        dispatch(editBook(data._id,data,data.index))
        reset();
        setEdit(false);
    } else {
        dispatch(addBook(data));
    }
  };

  useEffect(() => {
    dispatch(loadAllBook());
  }, []);

  const handleEditItem = (index,item) =>{
    const fields = ['_id','author', 'isbn','name'];
    fields.forEach(field => setValue(field, item[field]));
    setValue('index',index)
    setEdit(true);
  }
  const handleShowItem = (id) =>{
    dispatch(bookAuthor(id));
    setShow(true);
  }
  return (
    <>
      <h4> Book Form </h4>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input
          className="form-control"
          placeholder="name"
          {...register("name", { required: true })}
        />
        <input
          className="form-control"
          placeholder="ISBN"
          {...register("isbn", { required: true })}
        />
        <input
          className="form-control"
          placeholder="Author"
          {...register("author", { required: true })}
        />
        <button type="submit"> {edit ? 'Edit' : 'Submit'}</button>
      </form>
      <h4> List Of books </h4>

      <ListGroup>
        {books &&
          books.map((item, key) => (
            <div style={{ display: "flex" }}>
              <div style={{ flex: 1 }}>
                <ListGroup.Item key={key}>{item.name}</ListGroup.Item>
              </div>
              <Button onClick={()=>handleEditItem(key,item)} variant="warning">Edit</Button>
              <Button onClick={()=>handleShowItem(item._id)} variant="success">show</Button>
            </div>
          ))}
      </ListGroup>
      {show && <TableDetails/>}
    </>
  );
};
