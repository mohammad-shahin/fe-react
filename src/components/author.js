import { ListGroup, Button } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { loadAllAuthor, addAuthor, editAuthor } from "../state/reducers/actionsIndex";
import { useForm } from "react-hook-form";

export const Author = () => {
  const dispatch = useDispatch();
  const { register, handleSubmit, setValue, reset } = useForm();
  const { authors } = useSelector((state) => state.main);
  const [edit, setEdit] = useState(false);

  const onSubmit = (data) => {
    if(edit) {
        dispatch(editAuthor(data._id,data,data.index))
        reset();
        setEdit(false);
    } else {
        dispatch(addAuthor(data));
    }
  };
  useEffect(() => {
    dispatch(loadAllAuthor());
  }, []);

  const handleEditItem = (index,item) =>{
    const fields = ['_id','firstName', 'lastName'];
    fields.forEach(field => setValue(field, item[field]));
    setValue('index',index)
    setEdit(true);
  }
  return (
    <>
      <h4> Author Form </h4>
      <form onSubmit={handleSubmit(onSubmit)}>
        <input
          className="form-control"
          placeholder="firstName"
          {...register("firstName")}
        />
        <input
          className="form-control"
          placeholder="lastName"
          {...register("lastName", { required: true })}
        />
       <button type="submit"> {edit ? 'Edit' : 'Submit'}</button>
      </form>
      <h4> List Of Authors </h4>
      <ListGroup>
        {authors &&
          authors.map((item, key) => (
            <div style={{ display: "flex" }}>
              <div style={{ flex: 1 }}>
                <ListGroup.Item>  {item.firstName + " " + item.lastName} </ListGroup.Item>
              </div>
              <Button
                onClick={() => handleEditItem(key, item)}
                variant="warning"
              >
                Edit
              </Button>
            </div>
          ))}
      </ListGroup>
    </>
  );
};
