import actionTypes from "./actionTypes";

const LoadBookSuccess = (data)=>({
    type:actionTypes.BOOK_LOAD_SUCCESS,
    payload:data
})
const LoadAuthorSuccess = (data)=>({
    type:actionTypes.AUTHOR_LOAD_SUCCESS,
    payload:data
})
const addNewBookSuccess = (data)=>({
    type:actionTypes.ADD_NEW_BOOK,
    payload:data
})
const addNewAuthorSuccess = (data)=>({
    type:actionTypes.ADD_NEW_AUTHOR,
    payload:data
})
const editBookSuccess = (data)=>({
    type:actionTypes.UPDATE_BOOK,
    payload:data
})
const editAuthorSuccess = (data)=>({
    type:actionTypes.UPDATE_AUTHOR,
    payload:data
})
const bookAuthorSuccess = (data)=>({
    type:actionTypes.BOOK_AUTHOR,
    payload:data
})

const LoadBookError = (errorMessage)=>({
    type:actionTypes.BOOk_LOAD_ERROR,
    payload:errorMessage
})
export default {
    LoadBookSuccess,
    LoadBookError,
    LoadAuthorSuccess,
    addNewAuthorSuccess,
    addNewBookSuccess,
    editBookSuccess,
    editAuthorSuccess,
    bookAuthorSuccess
}

