import actionTypes from "./actionTypes";
import initialState from "./intialStates";

const mainReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionTypes.BOOK_LOAD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        books: payload.result,
      };
    case actionTypes.AUTHOR_LOAD_SUCCESS:
      return {
        ...state,
        authors: payload.result,
      };
    case actionTypes.ADD_NEW_AUTHOR:
      return {
        ...state,
        authors: [payload.result, ...state.authors],
      };
    case actionTypes.ADD_NEW_BOOK:
      return {
        ...state,
        books: [payload.result, ...state.books],
      };
    case actionTypes.UPDATE_AUTHOR:
      let { author } = payload
      return {
        ...state,
        authors: [
          ...state.authors.slice(0,payload.index,),
          author,
          ...state.authors.slice(payload.index+ 1),
        ],
      };
    case actionTypes.UPDATE_BOOK:
      const { book } = payload;
      return {
        ...state,
        books: [
          ...state.books.slice(0, payload.index),
          book,
          ...state.books.slice(payload.index + 1),
        ],
      };
    case actionTypes.BOOK_AUTHOR:
      return{
        ...state,
        bookDetails: payload && payload.newResult
      }
    default:
      return {
        ...state,
      }
  }
};
export default mainReducer;
