export default {
    isLoading:false,
    books:null,
    authors:null,
    errorMessage:null,
    userFound:false,
    bookDetails:{}
}