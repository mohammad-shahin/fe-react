export default {
    BOOK_LOAD_START: 'BOOK_LOAD_START',
    BOOK_LOAD_SUCCESS: 'BOOK_LOAD_SUCCESS',
    AUTHOR_LOAD_SUCCESS: 'AUTHOR_LOAD_SUCCESS',
    ADD_NEW_BOOK: 'ADD_NEW_BOOK',
    ADD_NEW_AUTHOR: 'ADD_NEW_AUTHOR',
    UPDATE_AUTHOR:'UPDATE_AUTHOR',
    UPDATE_BOOK: 'UPDATE_BOOK',
    BOOK_AUTHOR: 'BOOK_AUTHOR',
    BOOk_LOAD_ERROR: 'LOAD_ERROR',
}