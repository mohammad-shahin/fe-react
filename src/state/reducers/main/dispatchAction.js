import actions from "./actions";
import UserService from "../../../services/users";

export const loadAllBook = () => (dispatch) => {
  UserService.getAllBook()
    .then((response) => dispatch(actions.LoadBookSuccess(response.data)))
    .catch((error) => dispatch(actions.LoadBookError(error.message)));
};
export const addBook = (book) => (dispatch) => {
  UserService.addNewBook(book)
    .then((response) => dispatch(actions.addNewBookSuccess(response.data)))
    .catch((error) => dispatch(actions.LoadBookError(error.message)));
};
export const addAuthor = (author) => (dispatch) => {
  UserService.addNewAuthor(author)
    .then((response) => dispatch(actions.addNewAuthorSuccess(response.data)))
    .catch((error) => dispatch(actions.LoadBookError(error.message)));
};
export const editBook = (id, book, index) => (dispatch) => {
  UserService.editBook(id,book)
    .then((response) => dispatch(actions.editBookSuccess({result: response.data, book, index })))
    .catch((error) => dispatch(actions.LoadBookError(error.message)));
};
export const editAuthor = (id, author, index) => (dispatch) => {
  UserService.editAuthor(id,author)
    .then((response) => dispatch(actions.editAuthorSuccess({result: response.data, author, index })))
    .catch((error) => dispatch(actions.LoadBookError(error.message)));
};
export const bookAuthor = (id) => (dispatch) => {
    UserService.getBookAuthorById(id)
      .then((response) => dispatch(actions.bookAuthorSuccess(response.data)))
      .catch((error) => dispatch(actions.LoadBookError(error.message)));
  };
export const loadAllAuthor = () => (dispatch) => {
  UserService.getAllAuthor()
    .then((response) => dispatch(actions.LoadAuthorSuccess(response.data)))
    .catch((error) => dispatch(actions.LoadBookError(error.message)));
};