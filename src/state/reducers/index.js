import {combineReducers} from "redux"
import mainReducer from './main/reducers'
const reducers = combineReducers({
    main:mainReducer,
})
export default reducers;