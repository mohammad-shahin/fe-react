import {createStore,applyMiddleware} from 'redux'
import reducers from './reducers/index'
import thunk from 'redux-thunk'
const configuerStore = ()=>{
    const middleware = [thunk];
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_ 
}
export const store =createStore(reducers,{},applyMiddleware(thunk))