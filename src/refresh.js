import React, { useEffect } from "react";
import {useSelector,useDispatch} from 'react-redux';
import {getToken} from './state/reducers/actionsIndex';

export default function Refresh() {
    const dispatch = useDispatch();
    useEffect(() => {
        if (performance.navigation.type === 1) {
            dispatch(getToken())
        } 
  });

  return <div></div>;
}