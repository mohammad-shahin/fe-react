import axios from "axios";

const apiClient = () => {
	const { REACT_APP_API_URL } = process.env;
	const axiosInstance = axios.create({
		baseURL: 'http://localhost:8000/api/',
		responseType: "json",
		withCredentials: true
	});
	return axiosInstance;
};

export default apiClient;