import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Header, Book, Author } from './components'
function App(props) {
  return (
    <>
    <Header></Header>
    <div className="main-dev">

      <div className="book-side">
          <Book></Book>
      </div>
      <div className="author-side">
          <Author></Author>
      </div>

    </div>
    </>
  );
}

export default (App);
